from lexy import integer, string

from frasm.data import BaseDataType


class ByteType(BaseDataType):

    def __init__(self):
        super(ByteType, self).__init__('byte', integer, size=2)

    def validate(self, byte):
        if byte < 0 or byte > 255:
            raise ValueError('Invalid byte: {}'.format(byte))

    def encode(self, byte):
        return bytes([byte])

class BytesType(BaseDataType):

    def __init__(self):
        super(BytesType, self).__init__('bytes', string)

    def validate(self, string):
        pass

    def encode(self, bytes):
        return bytes.encode('latin-1')

class StringType(BaseDataType):

    def __init__(self):
        super(StringType, self).__init__('string', string)

    def validate(self, string):
        pass

    def encode(self, string):
        return '{}\x00'.format(string).encode('latin-1')

