#! /usr/bin/env python3

import struct
import sys

from frasm.assembler import BaseAssembler
from frasm.entry import main
from frasm.errors import AssemblyError
from frasm.opcode import BaseOpcodeType
from frasm.contrib.operands import (
    RegisterOperand, ImmediateOperand, MemoryOperand)
from frasm.contrib.data import ByteType, BytesType, StringType


class Opcode(BaseOpcodeType):

    opcode_struct = struct.Struct('>H')

    # Mapping operand size -> operand mask
    operand_masks = {
        4:  0xf,
        8:  0xff,
        12: 0xfff,
        16: 0xffff,
        32: 0xffffffff,
    }

    def __init__(self, mnemonic, base_opcode, *args, **kwargs):
        kwargs['size'] = 2
        super(Opcode, self).__init__(mnemonic, *args, **kwargs)
        self.base_opcode = base_opcode

    def encode(self, *operands):
        '''
        Combine a base opcode pattern and given operands.
        '''
        operands_bits = 0
        shifted_bits = 0
        for i, (op_value, op_type) in enumerate(zip(operands, self.operands)):
            if op_type.ignored:
                continue
            op_value = op_value & self.operand_masks[op_type.size]
            operands_bits = operands_bits << op_type.size | op_value
            shifted_bits += op_type.size
        # Opcode = 4 bits for most base opcodes and 12 for operands.
        operands_bits <<= 12 - shifted_bits
        return self.opcode_struct.pack(self.base_opcode | operands_bits)

    def __str__(self):
        return '{} ({:#04x})'.format(
            super(Opcode, self).__str__(),
            self.base_opcode
        )



class Assembler(BaseAssembler):

    PROGRAM_START = 0x200
    PROGRAM_END = 0xFFF

    def locate_sections(self):
        '''
        Set the starting address for each section.

        This one is straightforward, but this could be tricky for some other
        architecture due to alignment issues.
        '''
        section = self.sections['']
        # Chip-8 programs should start at 0x200 since emulators are allowed to
        # occupy the 512 lower bytes in memory.
        section.set_address(self.PROGRAM_START)
        if section.address + section.size > self.PROGRAM_END:
            raise AssemblyError(
                'Output program is too big ({} bytes)'.format(section.size),
                None)

    def write_binary(self, fp):
        '''
        Output the final binary.

        This one is simple since it only concatenates the content of sections in
        the output binary, but some other binary format may require headers, etc.
        '''
        self.sections[''].write(fp)


# Address on 12 bits
addr_op = ImmediateOperand(size=12, signed=False)
# 8 and 4 bits constants
byte_op = ImmediateOperand(size=8, signed=False)
nibble_op = ImmediateOperand(size=4, signed=False)

# Any general purpose register: v0-vf
gp_reg_op = RegisterOperand(
    {'v{}'.format(i): int(i, 16) for i in '0123456789abcdef'}, size=4
)

# These register are used only to distiguish opcodes that have the same
# mnemonic (take a look at how many "ld" opcodes we have below), thus each
# one is used alone and they do not have real associated identifier.

# Special v0, memory access, timer (delay and sound), key, font and BCD
# pseudo-registers.
v0_op, i_op, dt_op, st_op, k_op, f_op, b_op = (
    RegisterOperand({name: -1}, ignored=True)
    for name in 'v0 i dt st k f b'.split()
)
# The v0 register is used alone in the "jp v0, addr" opcode.

# The i register is the only one used to access random bytes in memory.
indirect_i_op = MemoryOperand(i_op, ignored=True)


opcodes = (
    Opcode('sys',   0x0000, addr_op),
    Opcode('cls',   0x00e0),
    Opcode('ret',   0x00ee),
    Opcode('jp',    0x1000, addr_op),
    Opcode('call',  0x2000, addr_op),
    Opcode('se',    0x3000, gp_reg_op, byte_op),
    Opcode('sne',   0x4000, gp_reg_op, byte_op),
    Opcode('se',    0x5000, gp_reg_op, gp_reg_op),
    Opcode('ld',    0x6000, gp_reg_op, byte_op),
    Opcode('add',   0x7000, gp_reg_op, byte_op),

    Opcode('ld',    0x8000, gp_reg_op, gp_reg_op),
    Opcode('or',    0x8001, gp_reg_op, gp_reg_op),
    Opcode('and',   0x8002, gp_reg_op, gp_reg_op),
    Opcode('xor',   0x8003, gp_reg_op, gp_reg_op),
    Opcode('add',   0x8004, gp_reg_op, gp_reg_op),
    Opcode('sub',   0x8005, gp_reg_op, gp_reg_op),
    Opcode('shr',   0x8006, gp_reg_op, gp_reg_op),
    Opcode('subn',  0x8007, gp_reg_op, gp_reg_op),
    Opcode('shl',   0x800e, gp_reg_op, gp_reg_op),

    Opcode('sne',   0x9000, gp_reg_op, gp_reg_op),
    Opcode('ld',    0xa000, i_op, addr_op),
    Opcode('jp',    0xb000, v0_op, addr_op),
    Opcode('rnd',   0xc000, gp_reg_op, byte_op),
    Opcode('drw',   0xd000, gp_reg_op, gp_reg_op, nibble_op),

    Opcode('skp',   0xe09e, gp_reg_op),
    Opcode('sknp',  0xe0a1, gp_reg_op),

    Opcode('ld',    0xf007, gp_reg_op, dt_op),
    Opcode('ld',    0xf00a, gp_reg_op, k_op),
    Opcode('ld',    0xf015, dt_op, gp_reg_op),
    Opcode('ld',    0xf018, st_op, gp_reg_op),
    Opcode('add',   0xf01e, i_op, gp_reg_op),
    Opcode('ld',    0xf029, f_op, gp_reg_op),
    Opcode('ld',    0xf033, b_op, gp_reg_op),
    Opcode('ld',    0xf055, indirect_i_op, gp_reg_op),
    Opcode('ld',    0xf065, gp_reg_op, indirect_i_op)
)

assembler = Assembler(
    sections = (),
    data_types = (ByteType(), BytesType(), StringType()),
    opcodes = opcodes,

    warning_lazy_opcode_size = True,
    case_sensitive = False,
)

if __name__ == '__main__':
    main(assembler, sys.argv[1:])
