class BaseOpcodeType:
    '''
    Base class to represent an available instruction.
    '''

    def __init__(self, mnemonic, *args, **kwargs):
        # Unlike a data type, a mnemonic doesn’t have to be unique as long as
        # operands can make the difference.
        self.mnemonic = mnemonic

        # Operands must be "Operand" subclasses instances.
        self.operands = args

        # Optional constant used to speed up the computation of resulting
        # opcode size.
        self.size = kwargs.get('size', None)

    def encode(self, *operands):
        '''
        Turn the instruction and its operands to a binary form. The result will
        be directly used in the binary output.
        If the "size" attribute is not defined, this method will be called once
        more to compute the size of the opcode.
        '''
        raise NotImplementedError()

    def is_matching(self, parsed_operands):
        '''
        Return if the given parsed operands match operands that are associated
        with this opcode.
        '''
        # Obviously, the same number of operands is needed.
        if len(parsed_operands) != len(self.operands):
            return False

        # Check that each parsed operands match with the corresponding expected
        # operand.
        for parsed_operand, expected_operand in zip(
            parsed_operands, self.operands
        ):
            if not expected_operand.is_matching(parsed_operand):
                return False

        return True

    def __str__(self):
        return '{} {}'.format(
            self.mnemonic,
            ', '.join(map(str, self.operands))
        )
