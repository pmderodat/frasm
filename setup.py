from setuptools import setup, find_packages

from frasm import VERSION

setup(
    name = 'FrASM',
    version = VERSION,
    description = 'Framework for quick assemblers',
    packages = find_packages(),

    install_requires = [
        'lexy',
        'funcparserlib',
    ],

    dependency_links = [
        'https://bitbucket.org/pmderodat/lexy/get/master.tar.bz2#egg=lexy-dev',
        'https://bitbucket.org/pmderodat/funcparserlib/get/default.tar.bz2#egg=funcparserlib-dev',
    ],
)
