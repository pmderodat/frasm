import funcparserlib.parser as fpl
from lexy import (
    Identifier, Symbol, Integer,
    identifier, symbol, integer
)
from lexy.token import Location

from frasm.errors import BadAssemblerError, AssemblyError
from frasm.operand import (
    OperandParser, ParsedOperand,
    parsers_counter,
    BaseOperandType
)
from frasm.utils import (
    get_sequence_location,
    merge_tokens,
    or_all
)


no_lexer_symbol = set()


#
# Common parsers
#

# Mere symbol
symbol_id = next(parsers_counter)
def _symbol_cleaner(tokens):
    return ParsedOperand(
        Location(tokens[0].location.start, tokens[1].location.end),
        symbol_parser,
        tokens[1].value
    )
symbol_parser = OperandParser(
    fpl.memoize(fpl.a(Symbol('@'))) + fpl.a(identifier) >> _symbol_cleaner,
    symbol_id, {'@', }
)

# Mere integer
integer_id = next(parsers_counter)
def _integer_cleaner(token):
    return ParsedOperand(
        token.location, integer_parser, token.value
    )
_integer_parser = fpl.a(integer)
integer_parser = OperandParser(
    fpl.memoize(_integer_parser) >> _integer_cleaner,
    integer_id, no_lexer_symbol
)

# Symbol + constant offset
symbol_offset_id = next(parsers_counter)
def _symbol_offset_cleaner(tokens):
    return ParsedOperand(
        Location(tokens[0].location.start, tokens[1].location.end),
        symbol_offset_parser,
        (tokens[0].value, tokens[1].value)
    )
symbol_offset_parser = OperandParser(
    (
        fpl.skip(fpl.a(Symbol('@')) + fpl.a(Symbol('('))) +
        fpl.a(identifier) + fpl.skip(fpl.a(Symbol('+'))) + fpl.a(integer) +
        fpl.skip(fpl.a(Symbol(')')))
    ) >> _symbol_offset_cleaner,
    symbol_offset_id, {'@', '(', ')', '+'}
)

register_id = next(parsers_counter)
def _register_cleaner(token):
    return ParsedOperand(
        token.location, register_parser, token.value
    )
register_parser = OperandParser(
    fpl.a(identifier) >> _register_cleaner,
    register_id, no_lexer_symbol
)


#
# Specific and parametrable memory access parser
#

memory_id = next(parsers_counter)
def memory_parser(nested_parsers):
    result_id = hash((
        memory_id,
        tuple(parser.hash for parser in nested_parsers)
    ))

    def _memory_cleaner(tokens):
        return ParsedOperand(
            Location(tokens[0].location.start, tokens[2].location.end),
            result_parser,
            tokens[1]
        )

    result_parser = OperandParser(
        (
            fpl.a(Symbol('[')) +
            or_all(parser.parser for parser in nested_parsers) +
            fpl.a(Symbol(']'))
        ) >> _memory_cleaner,
        result_id, {'[', ']'}
    )
    return result_parser



#
# ... and associated operand types
#

class ImmediateOperand(BaseOperandType):
    '''
    Operand that contain any kind of immediate (integer, symbol or symbol +
    offset).
    '''

    def __init__(self, size, signed=True, **kwargs):
        super(ImmediateOperand, self).__init__(
            (integer_parser, symbol_parser, symbol_offset_parser),
            size=size, **kwargs
        )
        self.name = 'immediate'

        # If True both signed and unsigned immediates are allowed; otherwise
        # only unsigned immediates are.
        self.signed = signed

    def is_matching(self, parsed_operand):
        return self.is_matching_parsers(parsed_operand)

    def convert(self, parsed_operand, assembler):
        if parsed_operand.parser == symbol_parser:
            # This is a symbol
            value = int(assembler.get_undefined_symbol(parsed_operand.value))
        elif parsed_operand.parser == symbol_offset_parser:
            # This is a symbol + offset
            value = int(assembler.get_undefined_symbol(
                parsed_operand.value[0])
            ) + parsed_operand.value[1]
        else:
            # This is an integer
            value = parsed_operand.value

        if not self.signed and value < 0:
            raise AssemblyError(
                'Negative immediates are not allowed here',
                parsed_operand.location
            )
        if not self.signed and value >= 2 ** self.size:
            raise AssemblyError(
                '{} unsigned bits cannot hold {}'.format(self.size, value),
                parsed_operand.location
            )
        if self.signed and (
            value < -2 ** (self.size - 1) or
            value >= 2 ** (self.size - 1)
        ):
            raise AssemblyError(
                '{} signed bits cannot hold {}'.format(self.size, value),
                parsed_operand.location
            )
        return value


class RegisterOperand(BaseOperandType):
    '''
    Operand that hold a register name.
    '''

    def __init__(self, registers, **kwargs):
        super(RegisterOperand, self).__init__((register_parser, ), **kwargs)
        self.name = 'register'

        # "registers" must be a mapping: register name -> any value you might
        # find useful.
        self.accepted_registers = registers
        self.is_case_insensitive = False

    def set_case_insensitive(self):
        # This function can be called many times, but the first time only is
        # sufficient.
        if self.is_case_insensitive:
            return
        self.is_case_insensitive = True

        lower_registers = {
            name.lower(): value
            for name, value in self.accepted_registers.items()
        }
        if len(lower_registers) != len(self.accepted_registers):
            raise BadAssemblerError(
                'Register names are ambiguous because of case insensitivity')
        self.accepted_registers = lower_registers

    def _get_sensitive_name(self, parsed_operand):
        if self.is_case_insensitive:
            return parsed_operand.value.lower()
        else:
            return parsed_operand.value

    def is_matching(self, parsed_operand):
        return (
            self.is_matching_parsers(parsed_operand) and
            self._get_sensitive_name(parsed_operand) in self.accepted_registers
        )

    def convert(self, parsed_operand, assembler):
        return self.accepted_registers[
            self._get_sensitive_name(parsed_operand)
        ]


class MemoryOperand(BaseOperandType):
    '''
    Decorative operand that represent a memory access.
    '''

    def __init__(self, nested_operand, **kwargs):
        super(MemoryOperand, self).__init__(
            (memory_parser(nested_operand.parsers), ),
            size=nested_operand.size, **kwargs
        )
        self.name = 'memory({})'.format(nested_operand.name)

        self.nested_operand = nested_operand

    def is_matching(self, parsed_operand):
        return (
            self.is_matching_parsers(parsed_operand) and
            self.nested_operand.is_matching(parsed_operand.value)
        )

    def convert(self, parsed_operand, assembler):
        return self.nested_operand.convert(parsed_operand.value, assembler)
