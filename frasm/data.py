class BaseDataType:
    '''
    Base class to represent a type of data that can be used outside
    instructions.
    '''

    def __init__(self, name, *args, **kwargs):
        # Of course, a data type name must be unique.
        self.name = name

        # List of tokens used to build the data.
        self.operands = args

        # Optional function used to check the validity of input data. 
        self.validator = kwargs.get('validator', None)

        # Optional constant used to speed up the computation of resulting data
        # size.
        self.size = kwargs.get('size', None)

    def validate(self, *operands):
        '''
        Check the validity of input data. It must raise a ValueError if input
        is invalid (and please give a meaningful error message!).
        '''
        raise NotImplementedError()

    def encode(self, *operands):
        '''
        Turn the input data to a binary form. The result will be directly used
        in the binary output.
        If "size" is not defined, this method will be called once more to
        compute the size of the data.
        '''
        raise NotImplementedError()
