import sys


class Section:
    '''
    Hold a section name, a mutable address, a mutable size and a content list.
    '''

    def __init__(self, name):
        self.name = name
        self.address = None
        self.size = 0
        self.content = []

    def add(self, item):
        self.content.append(item)
        self.size += item.get_size()

    def set_address(self, address):
        self.address = address
        for item in self.content:
            item.address = address
            address += item.get_size()

    def write(self, fp):
        for item in self.content:
            fp.write(item.encode())

class Data:
    '''
    Just hold a data type and mutable inputs.
    '''

    def __init__(self, type, location, *inputs):
        self.type = type
        self.location = location
        self.inputs = inputs

    def get_size(self):
        if self.type.size is not None:
            return self.type.size
        else:
            return len(self.encode())

    def encode(self):
        return self.type.encode(*self.inputs)

class Symbol:
    '''
    Just hold a name and a mutable address.
    '''

    def __init__(self, name, location):
        self.name = name
        self.location = location
        self.address = None

    def get_size(self):
        return 0

    def encode(self):
        return b''

    def __int__(self):
        if self.address is None:
            print(
                'Warning: undefined symbol: {}'.format(self.name),
                file=sys.stderr
            )
            return 0
        return int(self.address)

class Opcode:
    '''
    Just hold mutable operands and an opcode type.
    '''

    def __init__(self, type, location, parsed_operands=None, operands=None):
        self.type = type
        self.location = location
        self.address = None

        # After opcode selection and before addresses computation, we only have
        # freshly parsed operands.
        self.parsed_operands = parsed_operands

        # After opcode selection, the known operand types can be used to
        # convert parsed operands to real operands.
        self.operands = operands

    def get_size(self):
        if self.type.size is not None:
            return self.type.size
        else:
            return len(self.encode())

    def encode(self):
        # Force int conversion to resolve symbols.
        return self.type.encode(*self.operands)
