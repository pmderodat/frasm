from collections import namedtuple
from itertools import count


OperandParser = namedtuple('OperandParser', 'parser hash lexer_symbols')
ParsedOperand = namedtuple('ParsedOperand', 'location parser value')

parsers_counter = iter(count())


class BaseOperandType:
    '''
    Base class to represent an opcode operand type.

    This class exist only to explicitely define an interface. Use only its
    subclasses.
    '''

    def __init__(self, parsers, size=None, ignored=False):
        self.name = '<undefined>'
        self.parsers = parsers
        self.parser_ids = set(parser.hash for parser in self.parsers)
        self.size = size
        self.ignored = ignored
        self.assembler = None

    def set_case_insensitive(self):
        '''
        If needed, subclasses must make here any change to accept case
        insensitive input when it makes sense.
        '''
        pass

    def is_matching_parsers(self, parsed_operand):
        return parsed_operand.parser in self.parsers

    def is_matching(self, parsed_operand):
        '''
        Return if the given parsed operand matches self.

        The opcode used to represent an instruction is selected by looking at
        its operands. This method is used in this phase.
        '''
        raise NotImplementedError()

    def convert(self, parsed_operand, assembler):
        '''
        Convert a parsed operand to an operand value suitable for opcode
        encoding. Raise an Assembler if the parsed operand does not fit.

        This method is called when opcodes are selected and thus when addresses
        are known.
        '''
        raise NotImplementedError()

    def __str__(self):
        return self.name

