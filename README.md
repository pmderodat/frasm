FrASM — Assembler creation framework
=====================================

The need of an assembler for a special architecture is not so unusual. When
reverse-engineering some random machine or software, one often need to write an
assembler: this enables one to produce bytecodes in order to test a virtual
machine, or the behavior of some architecture.

FrASM aims to simplify and speed up the conception of simple assemblers: one
need only to provide a description of the instruction set and their operands,
maybe to write some hooks and to give opcode and binary format output code;
FrASM takes care of lexing, parsing, sections sorting, symbols resolution and
final binary output.

The framework is currently actively developped. Therefore, do not rely on the
(still) undocumented and moving API. ;-)
