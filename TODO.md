To implement
============

-   .align directives (in contrib or in core?)
-   a whole battery of tests!
-   enable data types to define a sub-grammar


To experiment
=============

-   strange opcodes (like dotted PowerPC ones)
    -   maybe split mnemonics parsing and opcodes handling like for operands
-   strange operands (like conditional instructions in ARM)
-   Very Long Instruction Word architectures: see how we can parse that, what
    principles such instructions rely on
